import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import CodeBlock from '@/components/general/CodeBlock'
import CodeBlockCss from '@/components/general/CodeBlockCss'
import VueCodeHighlight from 'vue-code-highlight'

Vue.config.productionTip = false

Vue.use(VueCodeHighlight)

Vue.component('code-block', CodeBlock)
Vue.component('code-block-css', CodeBlockCss)

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
