export default {
  data () {
    return {
      data: {
        request: {
          method: null,
          url: null,
          headers: null,
          content: null
        },
        response: {
          content: null,
          headers: null,
          status: null,
          statusText: null
        }
      }
    }
  }
}
