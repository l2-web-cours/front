export default {
  data () {
    return {
      typeBorder1: 'All',
      typeBorder2: 'All'
    }
  },
  methods: {
    onChangeDirection1 (event) {
      this.typeBorder1 = event
    },
    onChangeDirection2 (event) {
      this.typeBorder2 = event
    }
  }
}
