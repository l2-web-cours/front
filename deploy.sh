#!/bin/bash

set -e

TEMP=$(mktemp -d)
REPOSITORY=$(pwd)
WORKING_DIR="$TEMP/git-clone"
PASSWORD=$(cat ".env.local")

SERVER="jojotiquac@ssh.cluster005.hosting.ovh.net:~/l2/front/"

cd "$TEMP"
echo "Cloning repository"
git clone "$REPOSITORY" "$WORKING_DIR"
cd "$WORKING_DIR"
echo
echo "Switching to target build"
git checkout $1
echo "Installing npm dependencies"
npm install
echo "Packaging stuff"
npm run build:prod
echo "Remove old files"
sshpass -p "$PASSWORD" ssh -o StrictHostKeyChecking=no jojotiquac@ssh.cluster005.hosting.ovh.net 'rm -rf ~/l2/front/'
echo "Deploying files"
echo "Scp files to $SERVER"
sshpass -p "$PASSWORD" scp -o StrictHostKeyChecking=no -r dist/ jojotiquac@ssh.cluster005.hosting.ovh.net:~/l2/front/
echo "Copy htaccess"
sshpass -p "$PASSWORD" ssh -o StrictHostKeyChecking=no jojotiquac@ssh.cluster005.hosting.ovh.net 'cp ~/l2/.htaccess ~/l2/front/.htaccess'
echo "Delete temp directory"
rm -rf "$WORKING_DIR"
echo "Deployed successfully"
